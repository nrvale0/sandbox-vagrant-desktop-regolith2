#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue


function packages-install () {
    echo 'Installing required packages...'
    (set -x;
     export DEBIAN_FRONTEND=noninteractive;
     wget -qO - https://regolith-desktop.io/regolith.key \
	 | gpg --dearmor > /usr/share/keyrings/regolith-archive-keyring.gpg && \
	 echo deb "[arch=amd64 signed-by=/usr/share/keyrings/regolith-archive-keyring.gpg] https://regolith-desktop.io/release-ubuntu-jammy-amd64 jammy main" > /etc/apt/sources.list.d/regolith.list && \
	 apt-get update && apt-get install -y regolith-desktop)

    # The powermgmt stuff causes X to lock up on many VirtualBox VMs.
    (set -x;
     apt-get remove -y --purge \
	     xfce4-power-manager \
	     gnome-power-manager \
	     powermgmt-base)

    # Better with a display manager.
    (set -x;
     apt-get install -y lightdm && \
	 systemctl enable --now lightdm)
    
}


function system-config () {
    echo 'Install birdtray as smoketest for system tray...'
    (set -x;
     apt-get install -y birdtray)
}


function main () {
    packages-install
    system-config
}

main
